{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module BB4GG.Core ( BB4GGInstanceT
                  , BB4GGT
                  , MonadBB4GG (..)
                  , MonadBB4GGInstance (..)
                  , resetComputation
                  , runBB4GGInstanceT
                  , runBB4GGT
                  ) where

import BB4GG.Types     ( Instance (..)
                       , InstanceName
                       , InstanceState (..)
                       , State (..)
                       , currentComputationIS
                       , instancesS
                       , gamesIS
                       , stateI
                       , threadIdC
                       )
import BB4GG.Utilities (instanceDataPath, instanceNamesDataPath)

import Control.Concurrent          (killThread)
import Control.Concurrent.MVar     (MVar, modifyMVar_, readMVar, tryReadMVar)
import Control.Applicative         (Alternative)
import Control.Lens                (assign, use, view)
import Control.Monad               (MonadPlus)
import Control.Monad.Base          (MonadBase (..))
import Control.Monad.Except        (ExceptT)
import Control.Monad.Reader        (ReaderT (..), reader, runReaderT)
import Control.Monad.State         (MonadState (..), gets)
import Control.Monad.Trans         (MonadIO, MonadTrans, lift, liftIO)
import Control.Monad.Trans.Control ( ComposeSt
                                   , MonadBaseControl (..)
                                   , MonadTransControl (..)
                                   , defaultLiftBaseWith
                                   , defaultLiftWith
                                   , defaultRestoreM
                                   , defaultRestoreT
                                   )
import Data.Foldable               (forM_, mapM_)
import Data.List                   (intercalate)
import Data.Serialize              (encode)
import Snap.Internal.Core          (MonadSnap (..))

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict       as M


-- | A class that encapsulates all necessary functions to interact with the main
-- application. Interaction with the 'State' is via a 'MonadState'
-- interface.
class MonadState State m => MonadBB4GG m where
  saveState :: m () -- ^ Save the state (i.e. write it to disk).

-- | A monad transformer that adds 'MonadBB4GG' functionality to any monad that
-- instantiates 'MonadIO'.
newtype BB4GGT m a = BB4GGT
  { readerBT :: ReaderT (MVar State) m a
  } deriving
      ( Alternative
      , Applicative
      , Functor
      , Monad
      , MonadBase b
      , MonadIO
      , MonadPlus
      , MonadTrans
      )

-- | Run a 'BB4GGT'.
runBB4GGT :: BB4GGT m a -> MVar State -> m a
runBB4GGT = runReaderT . readerBT

instance MonadIO m => MonadState State (BB4GGT m) where
  get =
    BB4GGT . ReaderT $ liftIO . readMVar
  -- We need modifyMVar instead of putMVar here, probably for atomicity reasons.
  put s = BB4GGT . ReaderT $ liftIO . flip modifyMVar_ (const $ return s)

instance MonadIO m => MonadBB4GG (BB4GGT m) where
  saveState =
    do instances <- gets (intercalate "\n" . M.keys . view instancesS)
       liftIO $ writeFile instanceNamesDataPath instances

-- | Necessary for default 'MonadBaseControl' instance. Can not be derived
-- because of the associated type family.
instance MonadTransControl BB4GGT where
  type StT BB4GGT a = StT (ReaderT (MVar State)) a
  liftWith          = defaultLiftWith BB4GGT readerBT
  restoreT          = defaultRestoreT BB4GGT

-- | Necessary for 'MonadSnap' instance. Can not be derived because of the
-- associated type family.
instance MonadBaseControl b m => MonadBaseControl b (BB4GGT m) where
  type StM (BB4GGT m) a = ComposeSt BB4GGT m a
  liftBaseWith          = defaultLiftBaseWith
  restoreM              = defaultRestoreM

-- | Allows us to (automatically) lift from the 'Snap' monad.
instance MonadSnap m => MonadSnap (BB4GGT m) where
  liftSnap = BB4GGT . ReaderT . const . liftSnap

-- | A class that encapsulates all necessary functions to interact with an
-- instance. Interaction with the 'InstanceState' is via a 'MonadState'
-- interface.
class (MonadIO m, MonadState InstanceState m) => MonadBB4GGInstance m where
  instanceName      :: m InstanceName -- ^ Access to the name of the instance.
  saveInstanceState :: m ()           -- ^ Save the state (i.e. write it to disk).

instance MonadBB4GGInstance m => MonadBB4GGInstance (ExceptT e m) where
  instanceName = lift instanceName
  saveInstanceState = lift saveInstanceState

instance MonadBB4GGInstance m => MonadBB4GGInstance (ReaderT r m) where
  instanceName = lift instanceName
  saveInstanceState = lift saveInstanceState

-- | A monad transformer that adds 'MonadBB4GGInstance' functionality to any
-- monad that instantiates 'MonadIO'.
newtype BB4GGInstanceT m a = BB4GGInstanceT
  { readerBIT :: ReaderT (InstanceName, Instance) m a
  } deriving
      ( Alternative
      , Applicative
      , Functor
      , Monad
      , MonadBase b
      , MonadIO
      , MonadPlus
      , MonadTrans
      )

-- | Run a 'BB4GGInstanceT'.
runBB4GGInstanceT :: BB4GGInstanceT m a -> (InstanceName, Instance) -> m a
runBB4GGInstanceT = runReaderT . readerBIT

instance MonadIO m => MonadState InstanceState (BB4GGInstanceT m) where
  get =
    BB4GGInstanceT . ReaderT $ liftIO . readMVar . view stateI . snd
  -- We need modifyMVar instead of putMVar here, since putMVar waits until the
  -- MVar is empty
  put s =
    BB4GGInstanceT . ReaderT $
      liftIO . flip modifyMVar_ (const $ return s) . view stateI . snd

instance MonadIO m => MonadBB4GGInstance (BB4GGInstanceT m) where
  instanceName =
    BB4GGInstanceT $ reader fst
  saveInstanceState =
    do name <- instanceName
       liftIO
         . BS.writeFile (instanceDataPath name)
         . encode
         =<< use gamesIS

-- | Necessary for default 'MonadBaseControl' instance. Can not be derived
-- because of the associated type family.
instance MonadTransControl BB4GGInstanceT where
  type StT BB4GGInstanceT a = StT (ReaderT (InstanceName, Instance)) a
  liftWith                  = defaultLiftWith BB4GGInstanceT readerBIT
  restoreT                  = defaultRestoreT BB4GGInstanceT

-- | Necessary for 'MonadSnap' instance. Can not be derived because of the
-- associated type family.
instance MonadBaseControl b m => MonadBaseControl b (BB4GGInstanceT m) where
  type StM (BB4GGInstanceT m) a = ComposeSt BB4GGInstanceT m a
  liftBaseWith                  = defaultLiftBaseWith
  restoreM                      = defaultRestoreM

-- | Allows us to (automatically) lift from the 'Snap' monad.
instance MonadSnap m => MonadSnap (BB4GGInstanceT m) where
  liftSnap = BB4GGInstanceT . ReaderT . const . liftSnap


-- | Kills the currently running computation thread if there is one and resets
-- the computed assignments.
resetComputation :: MonadBB4GGInstance m => m ()
resetComputation =
  do mCompVar <- use currentComputationIS
     forM_ mCompVar $
       \ compVar ->
         do mComp <- liftIO $ tryReadMVar compVar
            liftIO . forM_ mComp $ mapM_ killThread . view threadIdC
     assign currentComputationIS Nothing
