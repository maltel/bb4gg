module BB4GG.Tests.RandomPlayerData (randomPlayers) where

import BB4GG.Types     ( GamePreference (..)
                       , Games
                       , PlayerData (..)
                       , Players
                       , Preference
                       , minPlayersGD
                       , maxPlayersGD
                       )
import BB4GG.Utilities (minPreference, maxPreference)

import Control.Lens              ((^.))
import Control.Monad             (replicateM)
import Control.Monad.Random.Lazy ( Rand
                                 , RandomGen
                                 , evalRand
                                 , getRandom
                                 , getRandomR
                                 , mkStdGen
                                 )
import Data.Traversable          (traverse)

import qualified Data.Map as M


-- | Returns a given number of players playing the given 'Games'. Every player
-- has a 50% chance to play a given game, and if they do their preference is
-- equally distributed between the minimum and maximum possible preference for a
-- game.
randomPlayers :: Int -> Games -> Players
randomPlayers num games =
  M.fromList
    . map
        ( \ (i, pref) ->
            ( i
            , PlayerData
                { _namePD = "Player" ++ show i
                , _inGamePD = False
                , _prefPD = pref
                }
            )
        )
    . zip [1..]
    . flip evalRand (mkStdGen 0)
    . replicateM num
    $ randomPreference games

-- | Returns a random 'Preference'. Every game has a 50% chance to be played,
-- and if it is the preference is equally distributed between the minimum and
-- maximum possible preference.
randomPreference :: RandomGen g => Games -> Rand g Preference
randomPreference games =
  M.mapMaybe id <$>
    traverse
      ( \ gData ->
          do plays <- getRandom
             score <- getRandomR (minPreference, maxPreference)
             if plays
               then
                 return . Just $
                   GamePreference
                     { _scoreGP = score
                     , _minPlayersGP = gData ^. minPlayersGD
                     , _maxPlayersGP = gData ^. maxPlayersGD
                     }
               else
                 return Nothing
      )
      games
