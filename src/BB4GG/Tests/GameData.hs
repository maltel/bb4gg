module BB4GG.Tests.GameData (games) where

import BB4GG.Types (GameData (..), Games)

import qualified Data.Map as M

{- Here are some games our gaming group has access to.
 - Some of the names are trade marks, so I am including some URLs for the games
 - which are commercially distributed and hope that the following list falls
 - under the terms of fair use.
 - (I preferred the webpages of the distributors in Germany since they are
 - usually whom we get the games from.)
 - If you think that some rights of yours are violated by the following list,
 - please consider contacting me via <arasergus[at]posteo.net> before
 - suing me -- removing the names is *a lot* easier for everyone than
 - going through a lawsuit.
 -}


-- | The default list of games.
games :: Games
games = M.fromList . zip [0 ..] $
  [ -- http://www.abacusspiele.de/spiele/hanabi/
    GameData
      { _nameGD   = "Hanabi"
      , _minPlayersGD = 3
      , _maxPlayersGD = 5
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://en.libellud.com/games/dixit
    GameData
      { _nameGD   = "Dixit"
      , _minPlayersGD = 4
      , _maxPlayersGD = 6
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- This is a "home-made" game which is played with Dixit (see above) cards.
    GameData
      { _nameGD   = "Dixit Contact"
      , _minPlayersGD = 4
      , _maxPlayersGD = 6
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://lookout-spiele.de/spiele/agricola/
    GameData
      { _nameGD   = "Agricola"
      , _minPlayersGD = 3
      , _maxPlayersGD = 5
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.dominion-welt.de/
    GameData
      { _nameGD   = "Dominion"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 2
      , _isAvailableGD = True
      }
  , -- https://www.ravensburger.de/shop/spiele/erwachsenenspiele/puerto-rico-26997/index.html
    GameData
      { _nameGD   = "Puerto Rico"
      , _minPlayersGD = 3
      , _maxPlayersGD = 5
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.abacusspiele.de/spiele/race-for-the-galaxy/
    GameData
      { _nameGD   = "Race for the Galaxy"
      , _minPlayersGD = 2
      , _maxPlayersGD = 5
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- https://www.heidelberger-spieleverlag.de/dyn/products/detail?ArtNr=he461
    GameData
      { _nameGD   = "Netrunner"
      , _minPlayersGD = 2
      , _maxPlayersGD = 2
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.hans-im-glueck.de/sankt-petersburg/
    GameData
      { _nameGD   = "St. Petersburg"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://rprod.com/index.php?page=description-22
    GameData
      { _nameGD   = "7 Wonders"
      , _minPlayersGD = 3
      , _maxPlayersGD = 7
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.7wondersduel.com
    GameData
      { _nameGD   = "7 Wonders Duel"
      , _minPlayersGD = 2
      , _maxPlayersGD = 2
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.pegasus.de/detailansicht/53005g-illuminati-2te-edition/
    GameData
      { _nameGD   = "Illuminati"
      , _minPlayersGD = 4
      , _maxPlayersGD = 6
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- https://www.amigo-spiele.de/spiel/saboteur
    GameData
      { _nameGD   = "Saboteur"
      , _minPlayersGD = 5
      , _maxPlayersGD = 10
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- https://www.heidelberger-spieleverlag.de/dyn/products/detail?ArtNr=HE445
    GameData
      { _nameGD   = "Resistance"
      , _minPlayersGD = 6
      , _maxPlayersGD = 10
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.adlung-spiele.de/Article?ArtNr=061018
    GameData
      { _nameGD   = "Kutschfahrt"
      , _minPlayersGD = 5
      , _maxPlayersGD = 10
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.pegasus.de/detailansicht/51325g-pandemie/
    GameData
      { _nameGD   = "Pandemic"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
--  , -- https://www.heidelberger-spieleverlag.de/shop/products/detail?ArtNr=CZ005
--    GameData
--      { _nameGD   = "Space Alert"
--      , _minPlayersGD = 4
--      , _maxPlayersGD = 5
--      , _isAvailableGD = True
--      }
--  , -- Fortunately (!), trade marks didn't exist back then.
--    GameData
--      { _nameGD   = "Go"
--      , _minPlayersGD = 2
--      , _maxPlayersGD = 2
--      , _isAvailableGD = True
--      }
--  , -- http://www.2f-spiele.de/spiele/funken_deu.htm
--    GameData
--      { _nameGD   = "Power Grid"
--      , _minPlayersGD = 4
--      , _maxPlayersGD = 5
--      , _isAvailableGD = True
--      }
--  , -- http://www.wham-o.com/product/frisbee_disc.html
--    GameData
--      { _nameGD   = "Frisbee"
--      , _minPlayersGD = 2
--      , _maxPlayersGD = 15
--      , _isAvailableGD = True
--      }
  , -- http://www.hans-im-glueck.de/carcassonne/carcassonne-grundspiel/
    GameData
      { _nameGD   = "Carcassonne"
      , _minPlayersGD = 2
      , _maxPlayersGD = 5
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.northstargames.com/products/evolution
    GameData
      { _nameGD   = "Evolution"
      , _minPlayersGD = 3
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.alderac.com/thunderstone/
    GameData
      { _nameGD   = "Thunderstone"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://ascensiongame.com/
    GameData
      { _nameGD   = "Ascension"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 2
      , _isAvailableGD = True
      }
  , -- http://www.abacusspiele.de/spiele/tichu/
    GameData
      { _nameGD   = "Tichu"
      , _minPlayersGD = 4
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://en.wikipedia.org/wiki/Charades
    GameData
      { _nameGD   = "Pantomime"
      , _minPlayersGD = 4
      , _maxPlayersGD = 10
      , _maxCopiesGD = 100
      , _isAvailableGD = True
      }
  , -- http://www.queen-games.com/en/2012/01/kingdom-builder-game-of-the-year-2012/
    GameData
      { _nameGD   = "Kingdom Builder"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  , -- http://www.pegasus.de/village/
    GameData
      { _nameGD   = "Village"
      , _minPlayersGD = 2
      , _maxPlayersGD = 4
      , _maxCopiesGD = 1
      , _isAvailableGD = True
      }
  ]
