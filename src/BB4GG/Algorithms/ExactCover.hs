module BB4GG.Algorithms.ExactCover (computeBestAssignments) where

import BB4GG.Types     ( Assignment
                       , Computation (..)
                       , GameID
                       , Games
                       , PlayerID
                       , Players
                       , InstanceState (..)
                       , currentAssigIS
                       , gamesIS
                       , inGamePD
                       , isAvailableGD
                       , playersIS
                       , prefPD
                       , maxCopiesGD
                       , maxPlayersGP
                       , minPlayersGP
                       )
import BB4GG.Utilities ( compareAssignments
                       , copiesInAssignment
                       , preferringPlayers
                       )

import Control.Concurrent.MVar (MVar, newMVar)
import Control.Lens        ((^.), view)
import Data.Ix             (inRange)
import Data.List           (sortBy, subsequences)
import Math.SetCover.Exact (Assign, assign, bitVectorFromSetAssigns, partitions)

import qualified Data.Map as M
import qualified Data.Set as S


-- | This computes the best 'Assignment's that are currently possible.
computeBestAssignments :: InstanceState -> Int -> IO (MVar Computation)
computeBestAssignments state num =
  newMVar $
    Computation
      { _threadIdC            = Nothing
      , _computedAssignmentsC = bestAssignments
      }
  where
    bestAssignments =
      take num
        . sortBy (compareAssignments (state ^. playersIS))
        . filter (not . null)
        $ allPlayableAssignments state

-- | This computes all 'Assignment's that are currently possible.
allPlayableAssignments :: InstanceState -> [Assignment]
allPlayableAssignments state = valids
  where
    valids =
      filter
        ( \ gs ->
            correctCopies currentAssig games gs && allPlayersPlaying players gs
        )
        labels
    labels =
      partitions $ bitVectorFromSetAssigns assigns
    assigns =
      getAssigns state
    currentAssig =
      state ^. currentAssigIS
    players =
      state ^. playersIS
    games =
      state ^. gamesIS

-- | This tests, maybe given the current assignment, if an assignment
-- fulfills the constraints on the number of instances of a game.
correctCopies ::
  Maybe Assignment -> Games -> Assignment -> Bool
correctCopies currentAssig games assig =
  all ( \ (gID, gd) ->
          copiesInAssignment (Just assig) gID
            <= (gd ^. maxCopiesGD) - copiesInAssignment currentAssig gID
      )
    $ M.toList games

-- | This tests if all players are playing in a given configration.
allPlayersPlaying :: Players -> Assignment -> Bool
allPlayersPlaying ps assig =
  S.unions (map snd assig)
    == (S.fromList . M.keys . M.filter (not . view inGamePD) $ ps)

-- | This computes all 'Assign's of 'PlayerID's to 'GameID's that are currently
-- possible.
getAssigns ::
  InstanceState -> [Assign (GameID, S.Set PlayerID) (S.Set PlayerID)]
getAssigns state = concatMap mkAssign gIDs
  where
    mkAssign gID = assigns
      where
        assigns =
          map
            ( \ l ->
                let pIDs = S.fromList $ map fst l
                in assign (gID, pIDs) pIDs
            )
            possibleOccupations
        possibleOccupations =
          filter sequenceLegal $ subsequences players
        players =
          M.toList . M.filter (not . view inGamePD) $ preferringPlayers ps gID
        sequenceLegal l =
          all (lengthLegalForPlayer $ length l) l
        lengthLegalForPlayer len (_, pd) =
          let gp = (pd ^. prefPD) M.! gID
          in inRange (gp ^. minPlayersGP, gp ^. maxPlayersGP) len
    ps   = state ^. playersIS
    gIDs = M.keys . M.filter (view isAvailableGD) $ state ^. gamesIS
