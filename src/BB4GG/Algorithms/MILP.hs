{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}

module BB4GG.Algorithms.MILP (computeBestAssignments) where

import BB4GG.Types     ( Assignment
                       , Computation (..)
                       , GameID
                       , PlayerID
                       , InstanceState (..)
                       , computedAssignmentsC
                       , currentAssigIS
                       , gamesIS
                       , inGamePD
                       , isAvailableGD
                       , playersIS
                       , prefPD
                       , maxCopiesGD
                       , maxPlayersGP
                       , minPlayersGP
                       , scoreGP
                       , threadIdC
                       )
import BB4GG.Utilities (copiesInAssignment, preferringPlayers)

import Control.Arrow           ((&&&))
import Control.Concurrent      (forkIO)
import Control.Concurrent.MVar (MVar, modifyMVar_, newMVar)
import Control.Lens            ((^.), makeLenses, over, set, view)
import Control.Monad           (guard, void)
import Data.LinearProgram      ( Direction (Max)
                               , GLPOpts (..)
                               , LPM
                               , VarKind (..)
                               , addObjective
                               , allVars
                               , equalTo'
                               , execLPM
                               , geq'
                               , glpSolveVars
                               , leq'
                               , leqTo'
                               , linCombination
                               , mipDefaults
                               , setDirection
                               , setVarKind
                               )
import Data.Either             (isRight, lefts, rights)
import Data.Foldable           (for_, traverse_)
import Data.List               (transpose)
import Data.Maybe              (fromMaybe)
import Data.Monoid             (Any (..))

import qualified Data.Map as M
import qualified Data.Set as S


-- | Encodes a game together with player ranges. Used as a variable in the
-- linear program that computes the best assignment.
data GameRangeVar = GameRangeVar
  { _gameIDGPV     :: GameID
  , _minPlayersGPV :: Int
  , _maxPlayersGPV :: Int
  } deriving (Eq, Ord, Read, Show)

makeLenses ''GameRangeVar

-- | Encodes a preference of a player. Used as a variable in the linear program
-- that computes the best assignment.
data PrefVar = PrefVar
  { _playerIDPV :: PlayerID
  , _gameRangeVarPV  :: GameRangeVar
  } deriving (Eq, Ord, Read, Show)

makeLenses ''PrefVar

-- | The type of a variable used in the linear program.
type Var = Either GameRangeVar PrefVar

-- | The result type of the linear program, mapping variables to their values in
-- an optimal solution.
type Result = M.Map Var Double


-- | The time limit for computing an assignment.
timeLimit :: Int
timeLimit = 60

-- | This computes the best 'Assignment's that are currently possible.
computeBestAssignments :: InstanceState -> Int -> IO (MVar Computation)
computeBestAssignments state num =
  do mVar <-
       newMVar $
         Computation
           { _threadIdC            = Nothing
           , _computedAssignmentsC = []
           }
     for_ (linearProgram state) $
       \ program ->
           do threadId <- forkIO $ computeNextBestAssignments num program mVar
              modifyMVar_ mVar $ return . set threadIdC (Just threadId)
     return mVar

-- | Computes the best n solutions of the given linear program where n is the
-- given integer and updates the 'Computation'.
computeNextBestAssignments ::
  Int -> LPM Var Int () -> MVar Computation -> IO ()
computeNextBestAssignments 0 _ mVar =
  modifyMVar_ mVar $ return . set threadIdC Nothing
computeNextBestAssignments num program mVar =
  do mResult <- runProgram program
     let mResultAssig =
           do result <- mResult
              assig <- extractAssignment result
              return (result, assig)
     case mResultAssig
       of Just (result, assig) ->
            do modifyMVar_ mVar $
                 return
                   . over computedAssignmentsC (++ [assig])
               computeNextBestAssignments (num - 1) (nextProgram result) mVar
          Nothing ->
            modifyMVar_ mVar $ return . set threadIdC Nothing
  where
    nextProgram result =
      do program
         -- cut off best solution
         let prefResult =
               M.filterWithKey
                 (\ var _ -> isRight var)
                 result
         leqTo' ("cut off previous best " ++ show num)
           ( linCombination
           $ M.foldMapWithKey
               (\ var value -> [(if value >= 0.5 then 1 else -1, var)])
               prefResult
           )
           (M.size (M.filter (>= 0.5) prefResult) - 1)

-- | Runs the given program and returns a potential 'Result'.
runProgram :: LPM Var Int () -> IO (Maybe Result)
runProgram program =
  do (_, mScoreResult) <-
       glpSolveVars (mipDefaults {tmLim = timeLimit}) $ execLPM program
     return $ fmap snd mScoreResult

-- | A helper function that cuts a list into pieces of a given length (though
-- the last one may be smaller).
divideList :: Int -> [a] -> [[a]]
divideList _ [] = []
divideList n l = take n l : divideList n (drop n l)

-- | Returns one of the possible assignments corresponding to the given
-- 'Result'.
extractAssignment :: Result -> Maybe Assignment
extractAssignment vars =
  -- This is an ugly fix for a weird bug where the algorithm returns solutions
  -- that have all variables equal to zero (even though this contradicts some of
  -- the constraints) when the problem has no solution.
  -- Somehow this does not occurr when the linear program is exported to a file
  -- and then ran directly through glpk, or even reimported into glpk-hs and run
  -- that way, which leads me to believe this is probably a bug in glpk-hs.
  if null playedGames
    then Nothing
    else Just $ concatMap gameAssignments playedGames
  where
    playedGames =
      lefts . M.keys $ M.filter (>= 0.5) vars
    -- this returns one of the equivalent possibilties of dividing the players
    -- onto the copies of the game
    gameAssignments game =
      map (const (view gameIDGPV game) &&& S.fromList)
        . transpose
        . divideList (round $ vars M.! Left game)
        $ playingPlayers game
    playingPlayers game =
      map (view playerIDPV)
        . filter ((== game) . view gameRangeVarPV)
        . rights
        . M.keys
        $ M.filter (>= 0.5) vars

-- | A helper type for determining the relevantly different ranges in the
-- potential player numbers of a game. It encompasses whether a player number
-- is a minimum and/or maximum of a relevantly different player range.
type MinMax = (Any, Any)

-- | Is used to say that a player number occurrs as the minimum of a relevantly
-- different player range.
minNum :: MinMax
minNum = (Any True, Any False)

-- | Is used to say that a player number occurrs as the maximum of a relevantly
-- different player range.
maxNum :: MinMax
maxNum = (Any False, Any True)

-- | Given a ascending list of player numbers together with indication if it
-- is the minimum and/or maximum of a range, computes the relevantly different
-- ranges. The first parameter is an internal parameter and should be @Nothing@
-- when called.
ranges :: Integral a => Maybe a -> [(a, MinMax)] -> [(a, a)]
ranges _ [] = []
ranges a ((b, m) : r)
  | m == minNum =
    maybe id ((:) . (, b - 1)) a $ ranges (Just b) r
  | m == maxNum =
    maybe id ((:) . (, b)) a $ ranges (Just $ b + 1) r
  | m == minNum <> maxNum =
    maybe id ((:) . (, b - 1)) a $ (b, b) : ranges (Just $ b + 1) r
  | otherwise =
    ranges a r

-- | Returns a linear program that computes the best currently possible
-- assignment (or @Nothing@ if there are no preferences to be fulfilled.
linearProgram :: InstanceState -> Maybe (LPM Var Int ())
linearProgram state =
  -- test that there are actually preferences to be fulfilled (otherwise might
  -- lead to crashes)
  condJust (not . null . rights . M.keys . allVars . execLPM) $
    do setDirection Max
       -- variables that represent the number of copies that are played of a
       -- certain game in certain player number range
       gameCopiesVars <-
         M.traverseWithKey
           ( \ gID _ ->
               traverse
                 ( \ gameRange ->
                     do setVarKind (Left gameRange) IntVar
                        return gameRange
                 )
                 (gamePlayerRanges gID)
           )
           games
       -- whether a player plays a certain game in a certain player range
       prefVars <-
         sequence $
           do (pID, pData) <- M.toList players
              (gID, pref) <- M.toList $ pData ^. prefPD
              gameCopiesVar <- fromMaybe [] $ gameCopiesVars M.!? gID
              guard $
                pref ^. minPlayersGP <= gameCopiesVar ^. minPlayersGPV
                  && gameCopiesVar ^. maxPlayersGPV <= pref ^. maxPlayersGP
              return $
                do let var = PrefVar pID gameCopiesVar
                   setVarKind (Right var) BinVar
                   addObjective $ monomial (pref ^. scoreGP) (Right var)
                   return var
       -- each player plays exactly one game
       for_ (M.keys players) $
         \ pID ->
           equalTo' ("player " ++ show pID ++ " plays exactly one game")
             ( linCombination
             . map ((1, ) . Right)
             . filter ((== pID) . view playerIDPV)
             $ prefVars
             )
             1
       -- at most the maximum number of copies of a game are played
       forWithKey_ games $
         \ gID gameData ->
           leqTo' ("game " ++ show gID ++ " is bounded by copies")
             ( linCombination
             . map ((1, ) . Left)
             $ gameCopiesVars M.! gID
             )
             ( gameData ^. maxCopiesGD
             - copiesInAssignment (state ^. currentAssigIS) gID
             )
       for_ gameCopiesVars
         . traverse_ $
             \ gameRange ->
               do let playerVars =
                        map Right $
                          filter
                            ((== gameRange) . view gameRangeVarPV)
                            prefVars
                      gameCopiesVar =
                        Left gameRange
                      playerNumFunc =
                        linCombination $ map (1, ) playerVars
                  -- a game is played by at least the minimum number of players
                  -- to get the number of copies full, and at most the maximum
                  -- number of players that fit in the number of copies
                  geq'
                    (  "game "
                    ++ show
                         ( gameRange ^. gameIDGPV
                         , gameRange ^. minPlayersGPV
                         , gameRange ^. maxPlayersGPV
                         )
                    ++ " min players"
                    )
                    playerNumFunc
                    (monomial (gameRange ^. minPlayersGPV) gameCopiesVar)
                  leq'
                    (  "game "
                    ++ show
                         ( gameRange ^. gameIDGPV
                         , gameRange ^. minPlayersGPV
                         , gameRange ^. maxPlayersGPV
                         )
                    ++ " max players"
                    )
                    playerNumFunc
                    (monomial (gameRange ^. maxPlayersGPV) gameCopiesVar)
  where
    games =
      M.filter (view isAvailableGD) $ state ^. gamesIS
    players =
      M.filter (not . view inGamePD) $ state ^. playersIS
    forWithKey_ m f =
      void $ M.traverseWithKey f m
    monomial c v =
      linCombination [(c, v)]
    condJust cond a
      | cond a = Just a
      | otherwise = Nothing
    gamePlayerRanges gID =
      do (minPlayers, maxPlayers) <-
           ranges Nothing
             . M.toAscList
             . M.fromListWith (<>)
             . concatMap
                 ( ( \ pref ->
                       [ (pref ^. minPlayersGP, minNum)
                       , (pref ^. maxPlayersGP, maxNum)
                       ]
                   )
                 . (M.! gID)
                 . view prefPD
                 )
             . M.elems
             $ preferringPlayers players gID
         return $
           GameRangeVar
             { _gameIDGPV     = gID
             , _minPlayersGPV = minPlayers
             , _maxPlayersGPV = maxPlayers
             }
