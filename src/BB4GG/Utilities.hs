module BB4GG.Utilities ( compareAssignments
                       , copiesInAssignment
                       , elemAt
                       , instanceDataPath
                       , instanceNamesDataPath
                       , insertNew
                       , minPreference
                       , mapWithIndex
                       , maxPreference
                       , preferenceList
                       , preferenceScore
                       , preferringPlayers
                       , sortMap
                       , trimWhitespace
                       ) where

import BB4GG.Types ( Assignment
                   , GameID
                   , InstanceName
                   , Players
                   , Score
                   , prefPD
                   , scoreGP
                   )

import Control.Lens  (view)
import Data.Char     (isSpace)
import Data.Function (on)
import Data.List     (sort)
import Data.Ord      (Down (..))
import Data.Tuple    (swap)

import qualified Data.Map.Strict as M
import qualified Data.Set        as S


-- | Path to the file containing the names of the instances.
instanceNamesDataPath :: FilePath
instanceNamesDataPath = "instances.dat"

-- | Path to the file containing the data of the given instance.
instanceDataPath :: InstanceName -> FilePath
instanceDataPath name = "_" ++ name ++ ".dat"

-- | Minimal possible preference score.
minPreference :: Score
minPreference = 1

-- | Maximal possible preference score.
maxPreference :: Score
maxPreference = 10

-- | Create a list of preference scores of given players for the given
-- assignment.
preferenceList :: Players -> Assignment -> [Score]
preferenceList players = concatMap gameScores
  where
    gameScores (gID, pIDs) =
      map ( view scoreGP
          . (M.! gID)
          . view prefPD
          . (players M.!)
          )
        $ S.toList pIDs

-- | Compute the total preference score of an assignment using the preferences
-- of given players.
preferenceScore :: Players -> Assignment -> Score
preferenceScore players = sum . preferenceList players

-- | Compares two 'Assignment's, the better one is smaller. Needs access to the
-- 'Players' to look up their preferences.
compareAssignments :: Players -> Assignment -> Assignment -> Ordering
compareAssignments players = compare `on` (Down . info)
  where
    info assignment =
      ( preferenceScore players assignment
      , minimum . (maxPreference :) . preferenceList players $ assignment
      )

-- | Extract those players who have a preference for the game with the given
-- ID from given players.
preferringPlayers :: Players -> GameID -> Players
preferringPlayers ps gID = M.filter (M.member gID . view prefPD) ps

-- | Count the number of copies of a given game in an assignment.
copiesInAssignment :: Maybe Assignment -> GameID -> Int
copiesInAssignment mAssignment gID =
  maybe 0 (length . filter ((== gID) . fst)) mAssignment

-- | A 'map'-like function which uses functions which take the index in the
-- list as an additional argument.
mapWithIndex :: (Int -> a -> b) -> [a] -> [b]
mapWithIndex f = zipWith f [0 ..]

-- | Add a new element to a map.
insertNew :: (Ord k, Num k) => a -> M.Map k a -> M.Map k a
insertNew x m = M.insert (maximum (0 : M.keys m) + 1) x m

-- | Create a list whose members are the results of applying the given
-- function to the key-value pairs of the given map and which is ordered
-- by the original values in the map.
sortMap :: (Ord k, Ord a) => ((k, a) -> b) -> M.Map k a -> [b]
sortMap f = map (f . swap) . sort . map swap . M.toList

-- | Trim whitespace from the beginning and the end of a string.
trimWhitespace :: String -> String
trimWhitespace = f . f
   where f = reverse . dropWhile isSpace

-- | A helper function returning the element at the specified index of a list if
-- it exists and @Nothing@ else.
elemAt :: Int -> [a] -> Maybe a
elemAt _ []     = Nothing
elemAt 0 (x:_)  = Just x
elemAt n (_:xs) = elemAt (n-1) xs
