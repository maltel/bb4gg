{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web (site) where

import BB4GG.Core                            ( MonadBB4GG
                                             , runBB4GGInstanceT
                                             )
import BB4GG.Interfaces.Web.DoActionByIDPage ( clearPlayersAction
                                             , deleteGameAction
                                             , deleteInstanceAction
                                             , deletePlayerAction
                                             , dismissAllAction
                                             , dismissGameAction
                                             , doInstanceActionByIDPage
                                             , doStateActionByIDPage
                                             , setCurrentAssignmentAction
                                             , toggleGameAction
                                             )
import BB4GG.Interfaces.Web.InstanceMainPage (instanceMainPage)
import BB4GG.Interfaces.Web.MainPage         (mainPage)
import BB4GG.Interfaces.Web.ModifyGamesPage  (modifyGamesPage)
import BB4GG.Interfaces.Web.ModifyPlayerPage (modifyPlayerPage)
import BB4GG.Interfaces.Web.ResultsPage      (resultsPage)
import BB4GG.Types                           (instancesS)

import Control.Lens        (use)
import Data.String         (fromString)
import Snap.Internal.Core  (MonadSnap)
import Snap.Core           (route)
import Snap.Util.FileServe (serveFile)

import qualified Data.Map.Strict as M


-- | Create a Snap site using the given state variable, mapping URLs to
-- corresponding actions.
site :: (MonadBB4GG m, MonadSnap m) => m ()
site =
  route
    . (++ [ ( ""
            , mainPage
            )
          , ( "instances/delete"
            , doStateActionByIDPage deleteInstanceAction
            )
          , ( "robots.txt"
            , serveFile "robots.txt"
            )
          ]
      )
    . M.foldMapWithKey
        (\ name instData -> [(fromString name, instanceRoute name instData)])
    =<< use instancesS
  where
    instanceRoute name inst =
      route
        . map (\ (path, page) -> (path, runBB4GGInstanceT page (name, inst)))
        $ [ ( ""
            , instanceMainPage
            )
          , ( "clear"
            , doInstanceActionByIDPage clearPlayersAction
            )
          , ( "games/modify"
            , modifyGamesPage
            )
          , ( "games/modify/delete"
            , doInstanceActionByIDPage deleteGameAction
            )
          , ( "games/modify/toggle"
            , doInstanceActionByIDPage toggleGameAction
            )
          , ( "player/modify"
            , modifyPlayerPage
            )
          , ( "player/modify/delete"
            , doInstanceActionByIDPage deletePlayerAction
            )
          , ( "results"
            , resultsPage
            )
          , ( "results/choose"
            , doInstanceActionByIDPage setCurrentAssignmentAction
            )
          , ( "results/dismiss-all"
            , doInstanceActionByIDPage dismissAllAction
            )
          , ( "results/dismiss"
            , doInstanceActionByIDPage dismissGameAction
            )
          ]
