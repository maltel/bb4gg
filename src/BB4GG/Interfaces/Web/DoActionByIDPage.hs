{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.DoActionByIDPage ( clearPlayersAction
                                             , deleteGameAction
                                             , deleteInstanceAction
                                             , deletePlayerAction
                                             , dismissAllAction
                                             , dismissGameAction
                                             , doInstanceActionByIDPage
                                             , doStateActionByIDPage
                                             , setCurrentAssignmentAction
                                             , toggleGameAction
                                             ) where

import BB4GG.Core                        ( MonadBB4GG (..)
                                         , MonadBB4GGInstance (..)
                                         , resetComputation
                                         )
import BB4GG.Interfaces.Web.WebUtilities (renderInstancePage, renderPage)
import BB4GG.Types                       ( GameID
                                         , InstanceName
                                         , PlayerID
                                         , computedAssignmentsC
                                         , currentAssigIS
                                         , currentComputationIS
                                         , gamesIS
                                         , inGamePD
                                         , instancesS
                                         , isAvailableGD
                                         , playersIS
                                         , prefPD
                                         )
import BB4GG.Utilities                   (elemAt)

import Control.Concurrent.MVar (tryReadMVar)
import Control.Lens            ( (^.)
                               , assign
                               , indices
                               , itraversed
                               , ix
                               , modifying
                               , use
                               )
import Control.Monad           (when)
import Control.Monad.Except    (ExceptT, runExceptT, throwError)
import Control.Monad.Reader    (ReaderT, ask, runReaderT)
import Control.Monad.Trans     (liftIO)
import Data.Monoid             ((<>))
import Data.String             (fromString)
import Snap.Blaze              (blaze)
import Snap.Core               (getQueryParam, redirect)
import Snap.Internal.Core      (MonadSnap)
import Text.Read               (readMaybe)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict       as M
import qualified Data.Set              as S
import qualified Text.Blaze.Html5      as H


-- | This type describes an action that can be executed by visiting a page.
-- @Maybe b@ is a parameter that was given as part of the url and then read to
-- something of type @b@. If this did not work, a @Nothing@ is passed.
-- @m@ is the inner monad everything lives in.
-- When there is an error it throws an error message via @ExceptT String@.
-- The returned @String@ is the page the user should be redirected to if the
-- action was succesful.
type Action b m = ReaderT (Maybe b) (ExceptT String m) String


-- | Executes an 'Action' given its inputs.
runAction :: Action b m -> Maybe b -> m (Either String String)
runAction action mB = runExceptT $ runReaderT action mB

-- | The page belonging to an 'Action' on an instance.
doInstanceActionByIDPage ::
     (MonadBB4GGInstance m, MonadSnap m, Read a)
  => Action a m -> m ()
doInstanceActionByIDPage action =
  do idBS <- getQueryParam "id"
     let mID  = (readMaybe . BS.unpack) =<< idBS
     result <- runAction action mID
     case result
       of Left err ->
            do name <- instanceName
               blaze . renderInstancePage name "Error" $ H.toHtml err
          Right redirectLocation ->
            do saveInstanceState
               redirect $ fromString redirectLocation

-- | The page belonging to an 'Action' on the state.
doStateActionByIDPage ::
  (MonadBB4GG m, MonadSnap m) => Action InstanceName m -> m ()
doStateActionByIDPage action =
  do idBS <- getQueryParam "id"
     let mID  = fmap BS.unpack idBS
     result <- runAction action mID
     case result
       of Left err ->
            blaze . renderPage "Error" $ H.toHtml err
          Right redirectLocation ->
            do saveState
               redirect $ fromString redirectLocation

-- | The 'Action' of deleting the 'Instance' of some name. The corresponding
-- file on disk is *not* deleted, which is on purpose, to help with accidental
-- deletions.
deleteInstanceAction :: MonadBB4GG m => Action InstanceName m
deleteInstanceAction =
  do name <- maybe (throwError "Could not parse instance name") return =<< ask
     modifying instancesS $ M.delete name
     return "/?modify=true"

-- | The 'Action' of deleting the 'Player' belonging to some 'PlayerID'.
deletePlayerAction :: MonadBB4GGInstance m => Action PlayerID m
deletePlayerAction =
  do pID <- maybe (throwError "Could not parse player ID") return =<< ask
     players <- use playersIS
     when (M.notMember pID players) $ throwError "Player not found"
     resetComputation
     modifying playersIS $ M.delete pID
     return "../.."

-- | The 'Action' of deleting all 'Players'.
clearPlayersAction :: MonadBB4GGInstance m => Action () m
clearPlayersAction =
  do resetComputation
     assign playersIS M.empty
     return "."

-- | The 'Action' of deleting the game belonging to some 'GameID'.
deleteGameAction :: MonadBB4GGInstance m => Action GameID m
deleteGameAction =
  do gID <- maybe (throwError "Could not parse game ID") return =<< ask
     games <- use gamesIS
     when (M.notMember gID games) $ throwError "Game not found"
     resetComputation
     modifying gamesIS $ M.delete gID
     modifying (playersIS . traverse . prefPD) $ M.delete gID
     return "."

-- | The 'Action' of toggling between a game being available and not.
toggleGameAction :: MonadBB4GGInstance m => Action GameID m
toggleGameAction =
  do gID <- maybe (throwError "Could not parse game ID") return =<< ask
     games <- use gamesIS
     when (M.notMember gID games) $ throwError "Game not found"
     resetComputation
     modifying (gamesIS . ix gID . isAvailableGD) not
     return "."

-- | The 'Action' of setting the current 'Assignment' to the one with the
-- given index.
setCurrentAssignmentAction :: MonadBB4GGInstance m => Action Int m
setCurrentAssignmentAction =
  do i <- maybe (throwError "Could not parse assignment index") return =<< ask
     compMVar <-
       maybe (throwError noAssigError) return =<< use currentComputationIS
     comp <-
       maybe (throwError noAssigError) return =<< liftIO (tryReadMVar compMVar)
     assig <-
       maybe (throwError "Assignment not found") return $
         elemAt i (comp ^. computedAssignmentsC)
     resetComputation
     assign
       ( playersIS
       . itraversed
       . indices (\ pID -> any (S.member pID . snd) assig)
       . inGamePD
       )
       True
     modifying currentAssigIS (<> Just assig)
     return ".."
  where
    noAssigError =
      "No assignments are known. Please visit the results page to \
      \(re)calculate them.\
      \"

-- | The 'Action' of removing a game from the current 'Assignment'.
dismissGameAction :: MonadBB4GGInstance m => Action Int m
dismissGameAction =
  do i <- maybe (throwError "Could not parse game index") return =<< ask
     assig <-
       maybe
         ( throwError
             "There is no current assignment, hence no game to dismiss."
         )
         return
         =<< use currentAssigIS
     (_, pIDs) <- maybe (throwError "Game not found") return $ elemAt i assig
     resetComputation
     assign
       (playersIS . itraversed . indices (`S.member` pIDs) . inGamePD)
       False
     assign
       currentAssigIS
       ( case take i assig ++ drop (i + 1) assig
           of [] -> Nothing
              l  -> Just l
       )
     return ".."

-- | The 'Action' of removing the current 'Assignment'.
dismissAllAction :: MonadBB4GGInstance m => Action () m
dismissAllAction =
  do assig <-
       maybe
         ( throwError
             "There is no current assignment, hence nothing to dismiss."
         )
         return
         =<< use currentAssigIS
     resetComputation
     assign
       ( playersIS
       . itraversed
       . indices (\ pID -> any (S.member pID . snd) assig)
       . inGamePD
       )
       False
     assign currentAssigIS Nothing
     return ".."
