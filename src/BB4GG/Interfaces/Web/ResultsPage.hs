{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.ResultsPage (resultsPage) where

import BB4GG.Core                        (MonadBB4GGInstance (..))
import BB4GG.Algorithms.MILP             (computeBestAssignments)
import BB4GG.Interfaces.Web.WebUtilities ( infoOf'
                                         , renderList
                                         , renderInstancePage
                                         , renderPlayerName
                                         )
import BB4GG.Types                       ( Assignment
                                         , InstanceState (..)
                                         , computedAssignmentsC
                                         , currentComputationIS
                                         , gamesIS
                                         , playersIS
                                         , threadIdC
                                         )
import BB4GG.Utilities                   ( mapWithIndex
                                         , maxPreference
                                         , preferenceList
                                         , preferenceScore
                                         , sortMap
                                         )

import Control.Concurrent      (threadDelay)
import Control.Concurrent.MVar (tryReadMVar)
import Control.Lens            ((^.), assign)
import Control.Monad           (when)
import Control.Monad.State     (get)
import Control.Monad.Trans     (MonadIO, liftIO)
import Data.List               (intersperse)
import Data.Maybe              (isJust, isNothing)
import Data.Monoid             (mconcat)
import Data.String             (fromString)
import Snap.Blaze              (blaze)
import Snap.Internal.Core      (MonadSnap)
import Text.Blaze              ((!))

import qualified Data.Map                    as M
import qualified Data.Set                    as S
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | The maximum number of best 'Assignment's the compute.
numAssignmentsToCompute :: Int
numAssignmentsToCompute = 10

-- | Time (in microseconds) to wait after starting a computation before showing
-- the result page.
timeToWaitForResult :: Int
timeToWaitForResult = 1000000

-- | The page where possible assignments are displayed.
resultsPage :: (MonadBB4GGInstance m, MonadSnap m) => m ()
resultsPage =
  do state <- get
     when (isNothing $ state ^. currentComputationIS) $
       do computationMVar <-
            liftIO $ computeBestAssignments state numAssignmentsToCompute
          assign currentComputationIS $ Just computationMVar
          liftIO $ threadDelay timeToWaitForResult
     name <- instanceName
     blaze . renderInstancePage name "Results" =<< renderResults =<< get

-- | Create an HTML segment with the 'numAssignmentsToCompute' many best-scoring
-- possible assignments.
renderResults :: MonadIO m => InstanceState -> m H.Html
renderResults state =
  case state ^. currentComputationIS
    of Just compMVar ->
         do mComp <- liftIO $ tryReadMVar compMVar
            case mComp
              of Just comp ->
                   return
                     . mconcat
                     . (H.h2 "Possible Assignments" :)
                     . ( \ l ->
                           case l
                             of [] -> []
                                _  -> [H.hr] ++ l ++ [H.hr]
                       )
                     . intersperse H.hr
                     . (++ [ H.h3 "Still computing..."
                           | isJust $ comp ^. threadIdC
                           ]
                       )
                     . mapWithIndex (renderAssignment state)
                     $ comp ^. computedAssignmentsC
                 Nothing ->
                   return $ H.h3 "Error: no computation running"
       Nothing ->
         return $ H.h3 "Error: no computation running"

-- | Create an HTML segment with an assignment.
renderAssignment ::
     InstanceState -- ^ Current state (required to compute scores).
  -> Int           -- ^ Index of the assignment in the list of assignments.
  -> Assignment    -- ^ Assignment to render.
  -> H.Html        -- ^ Resulting HTML segment.
renderAssignment state i assignment =
  mconcat $
    [ H.toHtml $
        "Score: "
          ++ (show . preferenceScore players $ assignment)
          ++ ", Minimum: "
          ++ ( show
             . minimum
             . (maxPreference :)
             $ preferenceList players assignment
             )
    , " - "
    , H.a
        ! A.href (fromString $ "choose?id=" ++ show i)
        $ "Choose this assignment"
    , H.br
    ]
      ++ map renderPair assignment
  where
    renderPair (gID, pIDs) =
      mconcat
        [ infoOf' H.b $ (state ^. gamesIS) M.! gID
        , ": "
        , renderList
            . sortMap (renderPlayerName . snd)
            . M.filterWithKey (\ pID _ -> S.member pID pIDs)
            $ players
        , H.br
        ]
    players =
      state ^. playersIS
