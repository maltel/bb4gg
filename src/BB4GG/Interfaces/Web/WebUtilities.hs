{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.WebUtilities ( availableClass
                                         , infoOf
                                         , infoOf'
                                         , inputNumber
                                         , isLegalName
                                         , labelOf
                                         , renderErrors
                                         , renderInstancePage
                                         , renderList
                                         , renderPage
                                         , renderPlayerName
                                         ) where

import BB4GG.Types     ( GameData (..)
                       , GameID
                       , InstanceName
                       , PlayerData (..)
                       , inGamePD
                       , isAvailableGD
                       , maxPlayersGD
                       , minPlayersGD
                       , nameGD
                       , namePD
                       )
import BB4GG.Utilities (trimWhitespace)

import Control.Lens   ((^.))
import Data.Char      (isAlphaNum, isLatin1)
import Data.List      (intersperse)
import Data.Monoid    (mconcat)
import Data.String    (IsString, fromString)
import Text.Blaze     ((!))
import Text.Digestive (Result (..), View, absoluteRef, fieldInputText)

import qualified Data.Set                    as S
import qualified Data.Text                   as T
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | Create an HTML page with given title and content,
-- adding the navigation menu.
renderInstancePage ::
     InstanceName -- ^ Name of the current instance.
  -> String       -- ^ Title of the page.
  -> H.Html       -- ^ Page content.
  -> H.Html       -- ^ Resulting page with the navigation menu.
renderInstancePage instName title content =
  renderPage (instName ++ " - " ++ title) $
    mconcat
      [ H.header $
          mconcat
            [ H.a
                ! A.href (fromString $ "/" ++ instName ++ "/")
                ! A.style linkStyle
                $ "Main"
            , " "
            , H.a
                ! A.href (fromString $ "/" ++ instName ++ "/results/")
                ! A.style linkStyle
                $ "Results"
            , " "
            , H.a
                ! A.href
                    (fromString $ "/" ++ instName ++ "/player/modify/")
                ! A.style linkStyle
                $ "Add Player"
            , " "
            , H.a
                ! A.href (fromString $ "/" ++ instName ++ "/games/modify/")
                ! A.style linkStyle
                $ "Modify Games"
            , H.hr
            ]
      , content
      ]
  where linkStyle = "padding: 0.5em; white-space: nowrap"

-- | Create an HTML page with given title and content.
renderPage ::
     String -- ^ Title of the page.
  -> H.Html -- ^ Page content.
  -> H.Html -- ^ Resulting page with the navigation menu.
renderPage title content =
  H.docTypeHtml $
    mconcat
      [ H.head $
          mconcat
            [ H.title . H.toHtml $ "BB4GG " ++ title
            , (H.meta ! A.name "viewport")
                ! A.content
                    "width=device-width, initial-scale=1.0, maximum-scale=1"
            , H.style ".notAvailable {color: DarkGrey; font-style: italic}"
            ]
      , H.body content
      ]

-- | Create a label from a game ID.
labelOf :: IsString a => GameID -> a
labelOf gID = fromString $ "game-" ++ show gID

-- | Create an information string about player number tresholds from game data.
tresholdInfoOf :: GameData -> String
tresholdInfoOf gd =
  if gd ^. minPlayersGD /= gd ^. maxPlayersGD
    then
      " ("
        ++ show (gd ^. minPlayersGD)
        ++ " - "
        ++ show (gd ^. maxPlayersGD)
        ++ ")"
    else
      " (" ++ show (gd ^. minPlayersGD) ++ ")"

-- | Create an information string containing the name of the game and its player
-- number tresholds from game data.
infoOf :: GameData -> String
infoOf gd = (gd ^. nameGD) ++ tresholdInfoOf gd

-- | Create an HTML snippet containing the game name which is modified
-- using the given function and player number tresholds from game data.
infoOf' ::
     (H.Html -> H.Html) -- ^ Function to apply to the name snippet.
  -> GameData           -- ^ Game data to be processed.
  -> H.Html             -- ^ Resulting HTML snippet.
infoOf' f gd =
  mconcat
    [ f . H.toHtml $ gd ^. nameGD
    , H.toHtml $ tresholdInfoOf gd
    ]

-- | Create an HTML snippet with an HTML5 number input.
inputNumber ::
     T.Text -- ^ Identifier for the number input.
  -> View v -- ^ View at hand.
  -> H.Html -- ^ Resulting snippet.
inputNumber ref view =
  H.input
    ! A.type_ "number"
    ! A.id    (H.toValue absRef)
    ! A.name  (H.toValue absRef)
    ! A.value (H.toValue (fieldInputText ref view))
  where absRef = absoluteRef ref view

-- | Concatenate a list of error snippets into one HTML snippet with red text,
-- adding full stops between the items.
renderErrors :: [H.Html] -> H.Html
renderErrors = (H.span ! A.style "color: red") . mconcat . intersperse ". "

-- | Check whether a name is legal.
isLegalName ::
     Int                  -- ^ Maximal length allowed.
  -> S.Set Char           -- ^ Legal special characters.
  -> S.Set String         -- ^ Names that are already taken.
  -> String               -- ^ Name to be checked.
  -> Result String String -- ^ An 'Error' with a description if the name is not
                          -- legal, a 'Success' with the trimmed version of the
                          -- name otherwise.
isLegalName maxLength special takenNames name
  | name' == "" =
    Error "Name can not be empty"
  | length name' > maxLength =
    Error $ "Name can not be longer than " ++ show maxLength ++ " characters"
  | isIllegal name' =
    Error "Name contains illegal characters"
  | name' `S.member` takenNames =
    Error "Name is already taken"
  | otherwise =
    Success name'
  where
    name' =
      trimWhitespace name
    isIllegal =
      not . all (\ c -> (isAlphaNum c && isLatin1 c) || c `S.member` special)

-- | Create an HTML class string indicating the availability of the given game
-- from game data.
availableClass :: IsString a => GameData -> a
availableClass gd =
  fromString $ if gd ^. isAvailableGD then "available" else "notAvailable"

-- | Create an HTML snippet with the name of of the given player which is
-- italized if the player is marked as "in game".
renderPlayerName :: PlayerData -> H.Html
renderPlayerName pd =
  (if pd ^. inGamePD then H.i else id) . H.toHtml $ pd ^. namePD

-- | Concatenate a list of HTML snippets in to one snippet, separating the items
-- with commas.
renderList :: [H.Html] -> H.Html
renderList = mconcat . intersperse ", "
