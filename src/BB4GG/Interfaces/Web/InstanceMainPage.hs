{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.InstanceMainPage (instanceMainPage) where

import BB4GG.Core                        (MonadBB4GGInstance (..))
import BB4GG.Interfaces.Web.WebUtilities ( availableClass
                                         , infoOf'
                                         , renderList
                                         , renderInstancePage
                                         , renderPlayerName
                                         )
import BB4GG.Types                       ( Assignment
                                         , GameData' (..)
                                         , PlayerData (..)
                                         , PlayerID
                                         , InstanceState (..)
                                         , currentAssigIS
                                         , gamesIS
                                         , playersIS
                                         , prefPD
                                         )
import BB4GG.Utilities                   ( mapWithIndex
                                         , preferringPlayers
                                         , sortMap
                                         )

import Control.Lens        ((^.), view)
import Control.Monad.State (get)
import Data.String         (fromString)
import Snap.Blaze          (blaze)
import Snap.Internal.Core  (MonadSnap)
import Text.Blaze          ((!))

import qualified Data.Foldable               as F
import qualified Data.Map.Strict             as M
import qualified Data.Set                    as S
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | The main page of the instance displaying the current assignment and the
-- current preferences.
instanceMainPage :: (MonadBB4GGInstance m, MonadSnap m) => m ()
instanceMainPage =
  do name <- instanceName
     blaze . renderInstancePage name "Main" . renderMain =<< get

-- | Display the main page.
renderMain :: InstanceState -> H.Html
renderMain state =
  mconcat
    [ H.h2 "Current Assignment"
    , case state ^. currentAssigIS
        of Nothing ->
             "N/A"
           Just assig ->
             mconcat
               [ renderCurrentAssignment state assig
               , H.p $
                   (H.a ! A.href "results/dismiss-all")
                     "Dismiss this assignment"
               ]
    , H.h2 "Current Preferences"
    , H.p
        "A name in italics means that the \
        \player is marked as \"in game\".\
        \"
    , H.p
        "Click on a player's name to modify \
        \their status or preferences.\
        \"
    , H.p $
        mconcat
          [ H.b "Players: "
          , renderList . sortMap (uncurry renderPlayerLink) $ state ^. playersIS
          ]
    , H.p $ renderPreferences state
    ]

-- | Display the current assignment.
renderCurrentAssignment :: InstanceState -> Assignment -> H.Html
renderCurrentAssignment state assig = mconcat (mapWithIndex renderPair assig)
  where
    renderPair i (gID, pIDs) =
      mconcat
        [ infoOf' H.b $ (state ^. gamesIS) M.! gID
        , ": "
        , renderList
            . sortMap (renderPlayerName . snd)
            . M.filterWithKey (\ pID _ -> pID `S.member` pIDs)
            $ state ^. playersIS
        , " - "
        , H.a ! A.href (fromString $ "results/dismiss?id=" ++ show i) $
            "Dismiss this game"
        , H.br
        ]

-- | Display the current preferences.
renderPreferences :: InstanceState -> H.Html
renderPreferences state =
  F.fold . sortMap renderGamePref . fmap GameData' $ preferredGames
  where
    renderGamePref (gID, gd') =
      mconcat
        [ H.span ! A.class_ (availableClass gd) $
            mconcat
              [ infoOf' H.b gd
              , ": "
              , renderList
                  . sortMap (renderPlayerName . snd)
                  $ preferringPlayers players gID
              ]
        , H.br
        ]
      where gd = gd'ToGD gd'
    preferredGames =
      M.filterWithKey
        (\ gID _ -> or . M.map (elem gID . M.keys . view prefPD) $ players)
        (state ^. gamesIS)
    players = _playersIS state

-- | Display the name of a player with a link to the page where you can modify
-- that player.
renderPlayerLink :: PlayerID -> PlayerData -> H.Html
renderPlayerLink pID = (H.a ! A.href value) . renderPlayerName
  where value = fromString ("player/modify/?id=" ++ show pID)
