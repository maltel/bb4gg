{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.ModifyGamesPage (modifyGamesPage) where

import BB4GG.Core                        ( MonadBB4GGInstance (..)
                                         , resetComputation
                                         )
import BB4GG.Interfaces.Web.WebUtilities ( availableClass
                                         , inputNumber
                                         , isLegalName
                                         , renderErrors
                                         , renderInstancePage
                                         )
import BB4GG.Types                       ( GameData (..)
                                         , GameID
                                         , Games
                                         , gamesIS
                                         , isAvailableGD
                                         , maxCopiesGD
                                         , maxPlayersGD
                                         , minPlayersGD
                                         , nameGD
                                         )
import BB4GG.Utilities                   ( insertNew
                                         , sortMap
                                         )

import Control.Applicative        ((<$>), (<*>))
import Control.Lens               ((^.), modifying, use, view)
import Data.Ix                    (inRange)
import Data.Maybe                 (fromJust)
import Data.String                (fromString)
import Snap.Blaze                 (blaze)
import Snap.Core                  (getQueryParam, redirect)
import Snap.Internal.Core         (MonadSnap)
import Text.Blaze                 ((!))
import Text.Digestive             ( Form
                                  , View
                                  , check
                                  , errors
                                  , string
                                  , stringRead
                                  , validate
                                  , (.:)
                                  )
import Text.Digestive.Blaze.Html5 (form, inputSubmit, inputText, label)
import Text.Digestive.Snap        (runForm)
import Text.Read                  (readMaybe)

import qualified Data.ByteString.Char8       as BS
import qualified Data.Map.Strict             as M
import qualified Data.Set                    as S
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | The page where you can modify the 'Games'.
modifyGamesPage :: (MonadBB4GGInstance m, MonadSnap m) => m ()
modifyGamesPage =
  do games <- use gamesIS
     midBS <- getQueryParam "id"
     let mgID  = (readMaybe . BS.unpack) =<< midBS
         mgd   = (`M.lookup` games) =<< mgID
     result <- runForm "modifyGames" $ modifyGamesForm mgd
     case result
       of (digestiveView, Nothing) ->
            do let digestiveView' = fmap H.toHtml digestiveView
               name <- instanceName
               blaze
                 . renderInstancePage name "Add Games"
                 . form digestiveView'
                     (fromString $
                        "?id="
                          ++ case mgID
                               of Just gID -> show gID
                                  Nothing  -> ""
                     )
                 $ modifyGamesView mgID games digestiveView'
          (_, Just newGameData) ->
            do resetComputation
               modifying
                 gamesIS
                 (case mgd
                    of Nothing -> insertNew newGameData
                       Just _  -> M.insert (fromJust mgID) newGameData
                 )
               saveInstanceState
               redirect "."

-- | The form for creating or modifying a game. If it is modifying a game,
-- the function gets passed the current 'GameData' so the fields can be
-- prefilled.
modifyGamesForm :: Monad m => Maybe GameData -> Form String m GameData
modifyGamesForm mgd =
  check "Min has to be <= Max" (\g -> g ^. minPlayersGD <= g ^. maxPlayersGD) $
    GameData
      <$> "gameName"
            .: validate (isLegalName 30 (S.fromList " _-:.'?!$%&") S.empty)
            (string $ fmap (view nameGD) mgd)
      <*> "min" .: check "Must be between 1 and 100" (inRange (1, 100))
            (stringRead "Not an integer" $ fmap (view minPlayersGD) mgd)
      <*> "max" .: check "Must be between 1 and 100" (inRange (1, 100))
            (stringRead "Not an integer" $ fmap (view maxPlayersGD) mgd)
      <*> "maxCopies" .: check "Must be between 1 and 100" (inRange (1, 100))
            ( stringRead "Not an integer"
            . Just
            $ maybe 1 (view maxCopiesGD) mgd
            )
      <*> pure (maybe True (view isAvailableGD) mgd)

-- | Displaying the 'Games' with options to modify them including the form for
-- creating or modifying a specific game.
modifyGamesView :: Maybe GameID -> Games -> View H.Html -> H.Html
modifyGamesView mgID gs digestiveView =
  mconcat
    [ case mgID
        of Just gID ->
             mconcat
               [ H.h2 "Modify a Game"
               , H.p $
                   H.a ! A.href (fromString $ "delete?id=" ++ show gID) $
                     "Delete this game"
               ]
           Nothing -> H.h2 "Add a Game"
    , H.p $
        mconcat
          [ let er = errors "" digestiveView
            in if null er
                 then mempty
                 else H.p $ renderErrors er
          , label "gameName" digestiveView "Name of the Game: "
          , inputText "gameName" digestiveView
          , " "
          , renderErrors $ errors "gameName" digestiveView
          , H.br
          , label "min" digestiveView "Minimum Number of Players: "
          , inputNumber "min" digestiveView
              ! A.min "1"
              ! A.max "100"
              ! A.style "width: 3em;"
          , " "
          , renderErrors $ errors "min" digestiveView
          , H.br
          , label "max" digestiveView "Maximum Number of Players: "
          , inputNumber "max" digestiveView
              ! A.min "1"
              ! A.max "100"
              ! A.style "width: 3em;"
          , " "
          , renderErrors $ errors "max" digestiveView
          , H.br
          , label
              "maxCopies"
              digestiveView
              "Maximum number of simultaneous instances: "
          , inputNumber "maxCopies" digestiveView
              ! A.min "1"
              ! A.max "100"
              ! A.style "width: 3em;"
          ]
    , H.p $ inputSubmit "Submit"
    , H.h2 "List of Games"
    , H.p
       "Click on a game to modify it or on the checkbox to toggle its\
       \ availibility status (unavailable games are greyed out).\
       \"
    , H.p $
        H.table $
          mconcat
            [ H.tr $
                mconcat
                  [ H.th mempty
                  , H.th "Name"
                  , H.th "Players"
                  , H.th "Copies"
                  ]
            , mconcat . sortMap (uncurry gameView) $ gs
            ]
    ]

-- | Display a game with a checkbox to toggle its availability.
gameView :: GameID -> GameData -> H.Html
gameView gID gd =
  H.tr ! A.class_ (availableClass gd) $
    mconcat
      [ H.td
          . (if gd ^. isAvailableGD then (! A.checked "true") else id)
          $ H.input
              ! A.type_ "checkbox"
              ! A.onclick
                  ( fromString $
                      "window.location.href = 'toggle?id="
                        ++ show gID
                        ++ "'; return false"
                  )
      , H.td $
          H.a
            ! A.href (fromString $ "?id=" ++ show gID)
            ! A.class_ (availableClass gd)
            $ H.toHtml $ gd ^. nameGD
      , H.td ! A.style alignStyle $
          H.toHtml $
            show (gd ^. minPlayersGD) ++ " - " ++ show (gd ^. maxPlayersGD)
      , H.td ! A.style alignStyle $ H.toHtml . show $ gd ^. maxCopiesGD
      ]
  where alignStyle = "text-align: center"
