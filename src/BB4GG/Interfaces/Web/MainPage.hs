{-# LANGUAGE OverloadedStrings #-}

module BB4GG.Interfaces.Web.MainPage (mainPage) where

import BB4GG.Core                        (MonadBB4GG (..))
import BB4GG.Interfaces.Web.WebUtilities (isLegalName, renderErrors, renderPage)
import BB4GG.Types                       ( Games
                                         , Instance (..)
                                         , InstanceName
                                         , InstanceState (..)
                                         , State (..)
                                         , instancesS
                                         )
import BB4GG.Utilities                   (instanceDataPath)

import Control.Concurrent.MVar    (newMVar)
import Control.Lens               ((^.), modifying)
import Control.Monad.State        (get)
import Control.Monad.Trans        (liftIO)
import Data.Serialize             (encode)
import Data.String                (fromString)
import Snap.Blaze                 (blaze)
import Snap.Core                  (getQueryParam, redirect)
import Snap.Internal.Core         (MonadSnap)
import Text.Blaze                 ((!))
import Text.Digestive             ( Form
                                  , Result
                                  , View
                                  , errors
                                  , string
                                  , validate
                                  , (.:)
                                  )
import Text.Digestive.Blaze.Html5 (form, inputSubmit, inputText)
import Text.Digestive.Snap        (runForm)

import qualified Data.ByteString.Char8       as BS
import qualified Data.Map.Strict             as M
import qualified Data.Set                    as S
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | The main page displaying the current assignment and the current
-- preferences.
mainPage :: (MonadBB4GG m, MonadSnap m) => m ()
mainPage =
  do state <- get
     mModify <- getQueryParam "modify"
     result <-
       runForm "newInstanceForm"
         . newInstanceForm
         . isLegalName 30 (S.fromList "-")
         . M.keysSet
         $ (state ^. instancesS)
     case result
       of (view, Nothing) ->
            blaze
              . renderPage "Home"
              . renderMain (mModify == Just "true") state
              $ fmap fromString view
          (_, Just name) ->
            do instanceState <-
                 liftIO $
                   newMVar
                     InstanceState
                       { _gamesIS              = M.empty
                       , _playersIS            = M.empty
                       , _currentComputationIS = Nothing
                       , _currentAssigIS       = Nothing
                       }
               modifying instancesS $
                 M.insert name Instance {_stateI = instanceState}
               liftIO
                 . BS.writeFile (instanceDataPath name)
                 $ encode (M.empty :: Games)
               saveState
               redirect "?modify=true"

-- | Display the main page.
renderMain ::
     Bool        -- ^ If modify mode is active.
  -> State       -- ^ The current state.
  -> View H.Html
  -> H.Html
renderMain isModify state view =
  mconcat
    [ H.h1 "Instances"
    , H.p $
        H.table $
          M.foldMapWithKey
            (\ name _ -> renderInstance isModify name)
            (state ^. instancesS)
    , if isModify
        then
          mconcat
            [ newInstanceView view
            , H.p $ H.a ! A.href "?" $ "Leave modify mode"
            ]
        else H.p $ H.a ! A.href "?modify=true" $ "Modify instances"
    ]

-- | Display a single instance.
renderInstance ::
     Bool         -- ^ If modify mode is active.
  -> InstanceName -- ^ The name of the instance.
  -> H.Html
renderInstance isModify name =
  H.tr ! A.style "vertical-align: text-bottom" $
    mconcat $
      H.td
        ( H.a ! A.href (fromString $ name ++ "/") $
            H.b ! A.style "font-size: x-large" $
              fromString name
        )
        : if isModify
            then [ H.td ! A.style "min-width: 1em" $ mempty
                 , H.td ! A.style "font-size: x-large" $
                     mconcat
                       [ "("
                       , H.a
                           ! A.href
                               (fromString $ "instances/delete?id=" ++ name)
                           $ "Delete"
                       , ")"
                       ]
                 ]
            else []

-- | The form for creating an instance.
newInstanceForm ::
  Monad m
  => (String -> Result String String) -- ^ The function passed to 'validate' to
                                      -- determine if the entered name is legal.
  -> Form String m String
newInstanceForm nameValidation =
  "name" .: validate nameValidation (string Nothing)

-- | Displaying the form for creating an instance.
newInstanceView :: View H.Html -> H.Html
newInstanceView view =
  H.p $
    form view "/?modify=true" $
      mconcat
        [ inputText "name" view
        , " "
        , renderErrors $ errors "name" view
        , H.br
        , inputSubmit "Create new instance"
        ]
