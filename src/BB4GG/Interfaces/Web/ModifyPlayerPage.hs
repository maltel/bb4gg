{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module BB4GG.Interfaces.Web.ModifyPlayerPage (modifyPlayerPage) where

import BB4GG.Core                        ( MonadBB4GGInstance (..)
                                         , resetComputation
                                         )
import BB4GG.Interfaces.Web.WebUtilities ( availableClass
                                         , infoOf
                                         , inputNumber
                                         , isLegalName
                                         , labelOf
                                         , renderErrors
                                         , renderInstancePage
                                         )
import BB4GG.Types                       ( GameData (..)
                                         , GameData' (..)
                                         , GameID
                                         , GamePreference (..)
                                         , Games
                                         , PlayerData (..)
                                         , PlayerID
                                         , Preference
                                         , gamesIS
                                         , inGamePD
                                         , playersIS
                                         , prefPD
                                         , maxPlayersGD
                                         , maxPlayersGP
                                         , minPlayersGD
                                         , minPlayersGP
                                         , namePD
                                         , scoreGP
                                         )
import BB4GG.Utilities                   ( insertNew
                                         , maxPreference
                                         , minPreference
                                         , sortMap
                                         )

import Control.Applicative        ((<$>), (<*>))
import Control.Lens               ((^.), modifying, use, view)
import Control.Monad              (liftM3)
import Data.Maybe                 (fromJust)
import Data.String                (fromString)
import Snap.Blaze                 (blaze)
import Snap.Core                  (getQueryParam, redirect)
import Snap.Internal.Core         (MonadSnap)
import Text.Blaze                 ((!))
import Text.Digestive             ( Form
                                  , Result (..)
                                  , View
                                  , absoluteRef
                                  , bool
                                  , check
                                  , childErrors
                                  , errors
                                  , string
                                  , validate
                                  , (.:)
                                  )
import Text.Digestive.Blaze.Html5 ( form
                                  , inputCheckbox
                                  , inputSubmit
                                  , inputText
                                  , label
                                  )
import Text.Digestive.Snap        (runForm)
import Text.Read                  (readMaybe)

import qualified Data.ByteString.Char8       as BS
import qualified Data.Foldable               as F
import qualified Data.Map.Strict             as M
import qualified Data.Set                    as S
import qualified Data.Text                   as T
import qualified Text.Blaze.Html5            as H
import qualified Text.Blaze.Html5.Attributes as A


-- | The page where you can create or modify a player.
modifyPlayerPage :: (MonadBB4GGInstance m, MonadSnap m) => m ()
modifyPlayerPage =
  do games <- use gamesIS
     players <- use playersIS
     midBS <- getQueryParam "id"
     let mpID  = (readMaybe . BS.unpack) =<< midBS
         mpd   = (`M.lookup` players) =<< mpID
     result <-
       runForm "modifyPlayerForm" $
         modifyPlayerForm
           mpd
           ( isLegalName
               20
               S.empty
               ( S.fromList
               . M.elems
               . fmap (view namePD)
               . maybe players (`M.delete` players)
               $ mpID
               )
           )
           games
     case result
       of (digestiveView, Nothing) ->
            do let digestiveView' = fmap H.toHtml digestiveView
               name <- instanceName
               blaze
                 . renderInstancePage name "Modify Player"
                 . form digestiveView'
                     ( fromString $
                         "?id="
                           ++ case mpID
                                of Just pID -> show pID
                                   Nothing  -> ""
                     )
                 $ modifyPlayerView mpID games digestiveView'
          (_ , Just newPlayerData) ->
            do resetComputation
               modifying
                 playersIS
                 ( case mpd
                     of Nothing -> insertNew newPlayerData
                        Just _  -> M.insert (fromJust mpID) newPlayerData
                 )
               redirect "../.."

-- | The form for creating or modifying a player.
modifyPlayerForm ::
  Monad m
  => Maybe PlayerData                 -- ^ For prefilling the fields when
                                      -- modifying a 'Player'.
  -> (String -> Result String String) -- ^ The function passed to 'validate' to
                                      -- determine if the entered name is legal.
  -> Games                            -- ^ The list of all games.
  -> Form String m PlayerData
modifyPlayerForm pd nameValidation gs =
  PlayerData
    <$> "name" .: validate nameValidation (string $ fmap (view namePD) pd)
    <*> "in-game" .: bool (fmap (view inGamePD) pd)
    <*> ( check "You must select at least one game" (not . M.null)
        . fmap (foldr M.union M.empty)
        . M.traverseWithKey (preferenceForm $ fmap (view prefPD) pd)
        $ gs
        )

-- | The form for entering the players 'Preference' for a single game. If a
-- player is being modified, the function gets passed the current 'Preference'
-- so the fields can be prefilled.
preferenceForm ::
  Monad m
  => Maybe Preference
  -> GameID
  -> GameData
  -> Form String m Preference
preferenceForm pref gID gd =
  fmap (M.fromList . F.toList . fmap (gID,))
    . (labelOf gID .:)
    . check "Min has to be <= Max"
        (maybe True (\ gp -> gp ^. minPlayersGP <= gp ^. maxPlayersGP))
    $ liftM3 GamePreference
        <$> "pref"
               .: validateRange "Preference"
                    (Just minPreference)
                    (Just maxPreference)
                    Nothing
                    (string $ fmap (show . view scoreGP) mgp)
  <*> "minP"
        .: validateRange "Min" (Just 1) Nothing (Just $ gd ^. minPlayersGD)
             (string minDefault)
  <*> "maxP"
        .: validateRange "Max" (Just 1) Nothing (Just $ gd ^. maxPlayersGD)
             (string maxDefault)
  where
    mgp =
      M.lookup gID =<< pref
    minDefault =
      ( ( \ mi ->
            if mi == gd ^. minPlayersGD
              then Nothing
              else Just $ show mi
        )
      . view minPlayersGP
      )
        =<< mgp
    maxDefault =
      ( ( \ ma ->
            if ma == gd ^. maxPlayersGD
              then Nothing
              else Just $ show ma
        )
      . view maxPlayersGP
      )
        =<< mgp
    validateRange name maybeMin maybeMax maybeDefault =
      validate
        ( \ x ->
            case readMaybe x
              of Nothing ->
                   if x == ""
                     then Success maybeDefault
                     else Error $ name ++ " is not an integer"
                 Just n ->
                   if maybe True (<= n) maybeMin && maybe True (n <=) maybeMax
                     then
                       Success $ Just n
                     else
                       Error $
                         name
                           ++ " is not in ["
                           ++ maybe "-inf" show maybeMin
                           ++ ", "
                           ++ maybe "inf" show maybeMax
                           ++ "]"
        )

-- | Displaying the data of the player with options to modify it.
modifyPlayerView :: Maybe PlayerID -> Games -> View H.Html -> H.Html
modifyPlayerView pID gs digestiveView =
  mconcat
  [ case pID
      of Just i ->
           mconcat
             [ H.h2 "Modify a Player"
             , H.p $
                 H.a ! A.href (fromString $ "delete?id=" ++ show i) $
                   "Delete this player"
             ]
         Nothing ->
           H.h2 "Add a Player"
  , H.p $
     mconcat
       [ label "name" digestiveView "Your Name: "
       , inputText "name" digestiveView
       , " "
       , renderErrors $ errors "name" digestiveView
       ]
  , H.p $
     mconcat
       [ inputCheckbox "in-game" digestiveView
       , label "in-game" digestiveView " Already in a game."
       ]
  , H.p $
     mconcat
       [ H.toHtml
           (  "You can enter integral preferences from "
           ++ show minPreference
           ++ " (lowest) to "
           ++ show maxPreference
           ++ " (highest) for the games you want to play.\
              \ You can also enter another range for the number of players\
              \ with which you would want to play a game.\
              \ Currently not available games are greyed out."
           )
       , H.br
       , H.toHtml
           (  "Clicking on a game will set its preference to "
           ++ show maxPreference
           ++ " if it has no preference yet and clear its\
              \ preference field otherwise."
           )
       ]
  , let er = errors "" digestiveView
    in if null er
         then mempty
         else H.p $ renderErrors er
  , H.p $
      H.table $
        mconcat
          [ H.tr $
              mconcat
                [ H.th "Pref"
                , H.th "Game"
                , H.th "Min"
                , H.th "Max"
                , H.th mempty
                ]
          , mconcat
              . sortMap
                  (\ (gID, gd) -> preferenceView gID (gd'ToGD gd) digestiveView)
              . fmap GameData'
              $ gs
          ]
  , H.p $ inputSubmit "Submit"
  , H.script ! A.type_ "text/javascript" $
      H.toHtml $
        "function setToMax(id) {\
        \  var el = document.getElementById(id);\
        \  if (el != null) {\
        \    if (el.value == '') {\
        \      el.value = '" ++ show maxPreference ++ "';\
        \    }\
        \    else {\
        \      el.value = '';\
        \    }\
        \  }\
        \}\
        \"
  ]

-- | Displaying the players preferences for a single game with options to
-- modify it.
preferenceView :: GameID -> GameData -> View H.Html -> H.Html
preferenceView gID gd digestiveView =
  H.tr ! A.class_ (availableClass gd) $
    mconcat
      [ H.td $
          inputNumber (fromString $ lbl ++ ".pref") digestiveView
            ! A.min (H.toValue minPreference)
            ! A.max (H.toValue maxPreference)
            ! A.style inputStyle
      , H.td $
          label (fromString lbl) digestiveView
            ! A.onclick
                ( fromString $
                    "setToMax('"
                       ++ T.unpack
                            ( absoluteRef
                                (fromString $ lbl ++ ".pref")
                                digestiveView
                            )
                       ++ "')"
                )
            $ H.toHtml $ infoOf gd
      , H.td $
          inputNumber (fromString $ lbl ++ ".minP") digestiveView
            ! A.min (H.toValue (1 :: Int))
            ! A.style inputStyle
      , H.td $
          inputNumber (fromString $ lbl ++ ".maxP") digestiveView
            ! A.min (H.toValue (1 :: Int))
            ! A.style inputStyle
      , H.td . renderErrors $ childErrors (fromString lbl) digestiveView
      ]
  where
    lbl        = labelOf gID
    inputStyle = "width: 3em"
