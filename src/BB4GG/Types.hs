{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE TemplateHaskell #-}

module BB4GG.Types where

import Control.Concurrent      (ThreadId)
import Control.Concurrent.MVar (MVar)
import Control.Lens            ((^.), makeLenses)
import Data.Function           (on)
import Data.Ord                (Down (..))
import Data.Serialize          (Serialize)
import Data.Word               (Word)
import GHC.Generics            (Generic)

import qualified Data.Map.Strict as M
import qualified Data.Set        as S


-- | Contains the data associated to a game.
data GameData = GameData
  { _nameGD        :: String -- ^ Name of the game.
  , _minPlayersGD  :: Int    -- ^ Minimum number of players for the game.
  , _maxPlayersGD  :: Int    -- ^ Maximum number of players for the game.
  , _maxCopiesGD   :: Int    -- ^ Number of copies available when the game is.
  , _isAvailableGD :: Bool   -- ^ Whether the game is available at all.
  } deriving (Eq, Show, Generic)

makeLenses ''GameData

instance Serialize GameData where

-- | Compares games first by their names, then the minimum number of
-- players and then the maximum number of players.
instance Ord GameData where
  compare = compare `on` info
    where info g = (g ^. nameGD, g ^. minPlayersGD, g ^. maxPlayersGD)

-- | A newtype wrapper for GameData that sorts for availability first
newtype GameData' = GameData' {gd'ToGD :: GameData} deriving Eq

instance Ord GameData' where
  compare = compare `on` info . gd'ToGD
    where info g = (Down $ g ^. isAvailableGD, g)

-- | Identifier for a game.
type GameID = Word

-- | Representation of multiple games.
type Games = M.Map GameID GameData

-- | Score for player preferences and assignments.
type Score = Int

-- | Contains preference data for a single game.
data GamePreference = GamePreference
  { _scoreGP      :: Score -- ^ Preference score.
  , _minPlayersGP :: Int   -- ^ Preferred number of minimum players.
  , _maxPlayersGP :: Int   -- ^ Preferred number of maximum players.
  } deriving (Eq, Show, Generic)

makeLenses ''GamePreference

instance Serialize GamePreference where

-- | Representation of preferences for multiple games.
type Preference = M.Map GameID GamePreference

-- | Contains the data associated to a player.
data PlayerData = PlayerData
  { _namePD   :: String     -- ^ Name of the player.
  , _inGamePD :: Bool       -- ^ Whether the player is marked as \"in game\".
  , _prefPD   :: Preference -- ^ Preferences of the player.
  } deriving (Eq, Show, Generic)

makeLenses ''PlayerData

instance Serialize PlayerData where

-- | Compares players first by whether they are marked as \"in game\",
-- then by their names.
instance Ord PlayerData where
  compare = compare `on` info
    where info pd = (pd ^. inGamePD, pd ^. namePD)

-- | Identifier of a game.
type PlayerID = Word

-- | Representation of multiple players.
type Players = M.Map PlayerID PlayerData

-- | Contains the data of an assignment of players to games.
type Assignment = [(GameID, S.Set PlayerID)]

-- | The computation of the currently best assignments. Contains the @ThreadId@
-- of the currently computing thread if there is one, as well as the already
-- computed assignments.
data Computation = Computation
  { -- | 'Just' the computing thread, 'Nothing' if it is already done
    _threadIdC            :: Maybe ThreadId
    -- | The already computed 'Assignment's
  , _computedAssignmentsC :: [Assignment]
  }

makeLenses ''Computation

-- | The state of a single instance of the application.
data InstanceState = InstanceState
  { -- | Games in question.
    _gamesIS              :: Games
    -- | Present players.
  , _playersIS            :: Players
    -- | 'Just' an MVar with the currently running computation if there is one,
    -- 'Nothing' otherwise.
  , _currentComputationIS :: Maybe (MVar Computation)
    -- | 'Just' the chosen assignment if there is one, 'Nothing' otherwise.
  , _currentAssigIS       :: Maybe Assignment
  }

makeLenses ''InstanceState

type InstanceName = String

-- | A single instance of the application.
data Instance = Instance
  { _stateI :: MVar InstanceState -- ^ The state of the instance.
  }

makeLenses ''Instance

-- | Multiple instances
type Instances = M.Map InstanceName Instance

-- | The state of the application.
data State = State
  { _instancesS :: Instances -- ^ The current instances.
  }

makeLenses ''State
