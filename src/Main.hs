{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections     #-}

module Main (main) where

import BB4GG.Core           (runBB4GGT)
import BB4GG.Interfaces.Web (site)
import BB4GG.Types          (State (..), Instance (..), InstanceState (..))
import BB4GG.Utilities      (instanceDataPath, instanceNamesDataPath)

--import BB4GG.Tests.RandomPlayerData (randomPlayers)

import Control.Concurrent.MVar (newMVar)
import Control.Monad           (unless)
import Data.Either             (rights)
import Data.Serialize          (decode)
import Snap.Http.Server        (quickHttpServe)
import System.Directory        (doesFileExist)
import System.IO.Error         (catchIOError, isDoesNotExistError)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map.Strict       as M


-- | This is an error handler that, if an error indicating the non-existence
-- of a file is raised, creates a new instance name file that is empty.
instanceNamesReadHandler :: IOError -> IO String
instanceNamesReadHandler e
  | isDoesNotExistError e =
    do writeFile instanceNamesDataPath ""
       return ""
  | otherwise =
    ioError e

-- | This is an error handler that, if an error indicating the non-existence
-- of a file is raised, instead returns the empty string.
instanceReadHandler :: IOError -> IO BS.ByteString
instanceReadHandler e
  | isDoesNotExistError e = return ""
  | otherwise             = ioError e

-- | Starting and running the server.
main :: IO ()
main =
  do instanceNames <-
       fmap lines $
         readFile instanceNamesDataPath `catchIOError` instanceNamesReadHandler
     instancesGames <-
       M.fromList . rights <$>
         mapM
           ( \ name ->
               fmap (fmap (name,) . decode)
                 . (`catchIOError` instanceReadHandler)
                 . BS.readFile
                 $ instanceDataPath name
           )
           instanceNames
     instances <-
       traverse
         ( \ games ->
             fmap (\ mVar -> Instance {_stateI = mVar})
               . newMVar
               $ InstanceState
                   { _gamesIS              = games
                   , _playersIS            = M.empty
                   , _currentComputationIS = Nothing
                   , _currentAssigIS       = Nothing
                   }
         )
         instancesGames
     stateVar <- newMVar State {_instancesS = instances}
     exists <- doesFileExist "robots.txt"
     unless exists $ writeFile "robots.txt" "User-agent: *\nDisallow: /\n"
     quickHttpServe $ runBB4GGT site stateVar
