module SingleToMultipleInstancesData (main) where

import Control.Monad      (when)
import System.Environment (getArgs)
import System.Directory   (renameFile)
import System.Exit        (die)


-- | The location of the file with the old game data.
gameDataPath :: FilePath
gameDataPath = "gameData.dat"

-- | Path to the file containing the names of the instances.
instanceNamesDataPath :: FilePath
instanceNamesDataPath = "instances.dat"

-- | Path to the file containing the data of the given instance.
instanceDataPath :: String -> FilePath
instanceDataPath name = "_" ++ name ++ ".dat"

-- | Converting the data from a single instance to the multiple instances
-- format. Expects the name of the new instance as a parameter.
main :: IO ()
main =
  do args <- getArgs
     when (null args) $ die "New instance name needed as argument."
     let instanceName = head args
     renameFile gameDataPath $ instanceDataPath instanceName
     writeFile instanceNamesDataPath instanceName
