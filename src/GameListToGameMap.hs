{-# LANGUAGE DeriveGeneric #-}

module GameListToGameMap (main) where

import Data.Serialize (Serialize, decode, encode)
import Data.Word      (Word)
import GHC.Generics   (Generic)

import qualified Data.ByteString.Char8 as BS
import qualified Data.Map              as M


type GameID = Word

-- | An old type we want to convert from.
data Game = Game
  { gameID      :: GameID
  , gameName    :: String
  , minPlayers  :: Int
  , maxPlayers  :: Int
  , maxCopies   :: Int
  , isAvailable :: Bool
  } deriving (Eq, Show, Generic)

instance Serialize Game where

-- | The new type we want to convert to.
data GameData = GameData
  { _nameGD        :: String
  , _minPlayersGD  :: Int
  , _maxPlayersGD  :: Int
  , _maxCopiesGD   :: Int
  , _isAvailableGD :: Bool
  } deriving (Eq, Show, Generic)

instance Serialize GameData where

-- | The location of the file with the game data.
gameDataPath :: String
gameDataPath = "gameData.dat"

-- | Conversion of single data points.
gameDataOf :: Game -> GameData
gameDataOf (Game _ name minP maxP copies avail) =
  GameData name minP maxP copies avail

-- | Conversion of the whole data structure.
gameListToGameMap :: [Game] -> M.Map GameID GameData
gameListToGameMap = M.fromList . map (\ g -> (gameID g, gameDataOf g))

-- | Doing the conversion.
main :: IO ()
main =
  do content <- BS.readFile gameDataPath
     case decode content
       of Right gs ->
            BS.writeFile gameDataPath . encode . gameListToGameMap $ gs
          Left str ->
            putStrLn str
